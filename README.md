# ansible-role-chromium-builder

This is an [Ansible Role][role] for setting up an enviroment, where Chromium
can be tested and built in an automated fashion.

[role]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html

## Synopsis

Below please find an example on how to set up a chromium environment using an Ansible Playbook:
	
```
- hosts:
    "chromium-servers"

  tasks:

    - import_tasks:
        "tasks/main.yml"
```